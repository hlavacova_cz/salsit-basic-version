function [par,s0,spar,res,ph_dif_corr]=LS_doubles(doubles,ph_dif,Dm,N1,N1d,log_folder,lambda,corrections)
%function [par,s0,spar,res,ph_dif_corr]=LS_doubles(doubles,ph_dif,Dm,N1,N1d,log_folder,lambda,corrections)
%part of the salsIt (small area least-squares InSAR tool) package
%performs the pairwise least-squares InSAR processing for all doubles specified by doubles matrix (matrix of point IDs)
%ph_dif are the (possibly wrapped) phase differences, while ph_dif_corr are phase differences corrected for estimated ambiguities
%log_folder is not mandatory, a folder where histograms are stored
%lambda is needed only for plotting into log_folder
%corrections (default 1) is not mandatory: if corrections=1, ambiguity corrections can be made
%can be also a vector with the length of the number of points (different value for each double)
%by IvCa, ivca@insar.cz, Jan/Feb 2018

if ~exist('corrections'),
	corrections=1;
end;

[k,n]=size(ph_dif); %k is the number of itfs, n is the number of doubles

if length(corrections)~=n,
	corrections=corrections(1)*ones(n,1);
end;


[i,K]=size(Dm); %K is the number of estimated parameters

par=zeros(K,n);
s0=zeros(1,n);
spar=zeros(K,n);
res=zeros(k,n);
ph_dif_corr=zeros(k,n);

cor=sqrt(diag(N1)); %correlation matrix

%ph_dif_corr=ph_dif;

for i=1:length(doubles),
%	fprintf(stderr,sprintf('ID: %d\n',i));
	[par(:,i),s0(i),spar(:,i),res(:,i),ph_dif_corr(:,i)]=LS_1(ph_dif(:,i),Dm,N1,N1d,corrections(i));
end;



if exist('log_folder')==1,
	if exist(log_folder)~=7,
		mkdir(log_folder);
	end;
	for i=1:K,
		figure();
		hist(par(i,:),100);
		if i==1,
			title('Velocity histogram for doubles');
			xlabel('Velocity [mm/y]');
			fname=sprintf('%s/hist_vel.png',log_folder);
		end;
		if i==2,
			title('Height correction histogram for doubles');
			ylabel('Height correction [m]');
			fname=sprintf('%s/hist_hc.png',log_folder);
		end;
		if i==3,
			hist(par(i,:)/4/pi*lambda,100); %i have to redo it here due to different measure
			title('Dilation coefficient histogram for doubles');
			ylabel('Dilation coefficient [mm/deg]');
			fname=sprintf('%s/hist_dilc.png',log_folder);
		end;
		f=gcf();
		print(f,fname,'-dpng','-r60');
		pause(1);
		close(f);
	end;
	figure();
	hist(s0/4/pi*lambda,100);
	title('Unique standard deviation histogram for doubles');
	xlabel('Unique standard deviation [mm]');
	fname=sprintf('%s/hist_s0.png',log_folder);
	f=gcf();
	print(f,fname,'-dpng','-r60');
	pause(1); %to have time to finish displaying the picture and to save it
	close(f);

	figure();
        hist(s0,100);
        title('Unique standard deviation histogram for doubles');
        xlabel('Unique standard deviation [rad]');
        fname=sprintf('%s/hist_s0_rad.png',log_folder);
        f=gcf();
        print(f,fname,'-dpng','-r60');
        pause(1); %to have time to finish displaying the picture and to save it
        close(f);




end;
