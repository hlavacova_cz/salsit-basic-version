function [width,height,bperp,dates,lambda,master_date,sl_r]=load_metadata(width_file,height_file,bperp_file,date_file,lambda_file,master_date_file,metadata_file,crop_file,rangeres_file)
%function [width,height,bperp,dates,lambda,master_date,sl_r]=load_metadata(width_file,height_file,bperp_file,date_file,lambda_file,master_date_file,metadata_file,crop_file,rangeres_file)
%part of the salsIt (small area least-squares InSAR tool) package
%loads the metadata (from stamps preprocessing) to be used in the following processing
%the output lambda is in mm, while the number in the input file is in m
%sl_r is the slant range to the (range) center of the crop
%width and height are the size of the crop to be processed
%by IvCa, ivca@insar.cz, Jan 2018

width=importdata(width_file);

height=importdata(height_file);

bperp=importdata(bperp_file);

d=importdata(date_file);

for i=1:length(d),
	dates(i)=datenum(num2str(d(i)),'yyyymmdd');
end;

lambda=importdata(lambda_file)*1000;


md=importdata(master_date_file);

master_date=datenum(num2str(md),'yyyymmdd');

range_res=importdata(rangeres_file);

f=fopen(metadata_file,'r');
c=textscan(f,'%s');
q=c{1};
for i=1:length(q),
	p=strfind(q(i),'slantrange');
	if length(p{1})>0,
		near_range=sscanf(q{i}(p{1}+11:end),'%f');
	end;
end;
fclose(f);


f=fopen(crop_file);
c=textscan(f,'%s');
q=c{1};
for i=1:length(q),
        p=strfind(q(i),'SMIN');
        if length(p{1})>0,
                smin=sscanf(q{i}(p{1}+5:end),'%d');
        end;
	p=strfind(q(i),'SMAX');
        if length(p{1})>0,
                smax=sscanf(q{i}(p{1}+5:end),'%d');
        end;
end;
fclose(f);

%disp([near_range range_res smin smax]);

sl_r=near_range+range_res*(smin+smax)/2;

