function export_results(pts,lat,lon,orig_height,par_pt,res_pt,s0_pt,spar_pt,coh_pt,mean_ampl,d_a,dates,lambda,master_date,id_rp,param_file,res_file,movres_file,model_file,refpt_file,s0_thr,coh_thr)
%function export_results(pts,lat,lon,orig_height,par_pt,res_pt,s0_pt,spar_pt,coh_pt,mean_ampl,d_a,dates,lambda,master_date,id_rp,param_file,res_file,movres_file,model_file,refpt_file,s0_thr,coh_thr)
%part of the salsIt (small area least-squares InSAR tool) package
%script used to export the results: parameters into param_file, pure residues time series into res_file, linear model into model_file, refpoint position into refpt_file
%should work both for lat/lon as a matrix, and lat/lon as the vectors (only for processed points)
%res_pt is in rad, here it is converted and exported to mm
%s0 value is exported in mm, however s0_thr (the threshold over which points are not dealt with) is in rad
%s0_thr is not mandatory (default inf, all points considered), the same applies for coh_thr
%both thresholds are applied together (AND)
%by IvCa, ivca@insar.cz, Feb 2018


%SHALL WE ALSO EXPORT SOMEWHERE the excluded points?


[n_pt,xx]=size(pts);

[rl cl]=size(lat);

if rl==n_pt && cl==1,
	lat_pt=lat;
	lon_pt=lon;
else
	lat_pt=zeros(n_pt,1);
	lon_pt=zeros(n_pt,1);

	for i=1:n_pt,
		lat_pt(i)=lat(pts(i,1),pts(i,2));
		lon_pt(i)=lon(pts(i,1),pts(i,2));
	end;
end;


height_pt=zeros(n_pt,1);
ma_pt=zeros(n_pt,1);
da_pt=zeros(n_pt,1);

for i=1:n_pt,
%	lat_pt(i)=lat(pts(i,1),pts(i,2));
%	lon_pt(i)=lon(pts(i,1),pts(i,2));
	height_pt(i)=orig_height(pts(i,1),pts(i,2));
	ma_pt(i)=mean_ampl(pts(i,1),pts(i,2));
	da_pt(i)=d_a(pts(i,1),pts(i,2));
	
end;

%correcting the heights

heicorr_pt=height_pt+par_pt(2,:)';

%disp(n_pt);

if exist('s0_thr') && (s0_thr~='-') && s0_thr>0 && ~exist('coh_thr'),
	exp_ind=find(s0_pt<=s0_thr);
%	disp('s0 thresholding')
%	disp([length(exp_ind) s0_thr]);
end;

if ~exist('s0_thr') && ~exist('coh_thr'),
	exp_ind=(1:n_pt);
end;


if exist('coh_thr') && ~exist('s0_thr'),
	exp_ind=find(coh_pt>=coh_thr);
%	disp('coh thresholding');
%	disp([length(exp_ind) coh_thr]);
end;

if exist('s0_thr') && (s0_thr~='-') && s0_thr>0 && exist('coh_thr'),
	exp_ind=find((s0_pt<=s0_thr).*(coh_pt>=coh_thr));
end;


header={'ID','LAT','LON','HEIGHT','SAMPLE','LINE','AMPL_MEAN','AMPL_DISP','VEL','HEIGHT_CORR','S_VEL','S_HEI','S0[mm]','COH'};

formatstring='%d,%.6f,%.6f,%.1f,%d,%d,%.2f,%.2f,%.2f,%.1f,%.2f,%.1f,%.2f,%.2f\n';

data=[exp_ind,lat_pt(exp_ind),lon_pt(exp_ind),heicorr_pt(exp_ind),pts(exp_ind,2),pts(exp_ind,1),ma_pt(exp_ind),da_pt(exp_ind),par_pt(1,exp_ind)',par_pt(2,exp_ind)',spar_pt(1,exp_ind)',spar_pt(2,exp_ind)',s0_pt(exp_ind)/4/pi*lambda,coh_pt(exp_ind)]';

export_csv(data,header,formatstring,param_file);


%exporting time series

header{1}='ID';
formatstring='%d,';

%adding master to the vectors and sorting;
dtmst=[dates,master_date];
[dates_mst,dat_ind]=sort(dtmst);
res_mst=[res_pt(:,exp_ind);zeros(1,length(exp_ind))];
res_pt_mst=res_mst(dat_ind,:)/4/pi*lambda;  %here is the conversion to mm

for i=1:length(dates_mst),
	header{i+1}=datestr(dates_mst(i),'yyyy-mm-dd');
	formatstring=strcat(formatstring,'%.1f,');
end;

formatstring=formatstring(1:end-1); %taking away the last comma
formatstring=strcat(formatstring,'\n');


%disp(formatstring);


%first, the residues

export_csv([exp_ind';res_pt_mst],header,formatstring,res_file);

%now, the model

model=(par_pt(1,exp_ind))'*(dates_mst-master_date)/365;


export_csv([exp_ind';model'],header,formatstring,model_file);

%and now, the residues with movement

export_csv([exp_ind';model'+res_pt_mst],header,formatstring,movres_file);


%and now, the position of the ref point

rp=pts(id_rp,:);
header={'ID','SAMPLE','LINE','LAT','LON','HEIGHT'};
formatstring='%d,%d,%d,%.6f,%.6f,%.1f';

data=[id_rp;pts(id_rp,2); pts(id_rp,1); lat_pt(id_rp);lon_pt(id_rp);height_pt(id_rp)];

export_csv(data,header,formatstring,refpt_file);
