function w=wrap(ph)
%function w=wrap(ph)
%wraps the phase into the <-pi,pi) interval
%works in radians
%works for vectors

w=ph;

for i=1:length(w),
	while w(i)>=pi,
		w(i)=w(i)-2*pi;
	end;

	while w(i)<-pi,
		w(i)=w(i)+2*pi;
	end;
end;
