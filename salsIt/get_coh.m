function coh=get_coh(res_pt,hist_file)
%function coh=get_coh(res_pt,hist_file)
%part of the salsIt (small area least-squares InSAR tool) package
%computes the coherence from residues for each point
%possibly it outputs the coherence histogram to a file (not mandatory)
%by IvCa, ivca@insar.cz, Feb 2018


[n_itf,n_pt]=size(res_pt);

coh=zeros(n_pt,1);

for i=1:n_pt,
	coh(i)=abs(sum(exp(-j*res_pt(:,i)))/n_itf);
end;


if exist('hist_file')==1,
	figure();
	hist(coh,100);
	title('Coherence histogram');
	xlabel('Coherence');
	f=gcf();
	print(f,hist_file,'-dpng','-r60');
	pause(1);
	close(f);
end;
