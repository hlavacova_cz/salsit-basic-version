function amb=amb_corr_it(res)
%function amb=amb_corr_it(res)
%performs ambiguity correction for one date and one point
%res are the residues (in radians)
%by IvCa, ivca@insar.cz, Jan/Feb 2015


if max(abs(res))>5
	amb=(abs(res)>5).*sign(res);
	return;
end;


if max(abs(res))>4
	amb=(abs(res)>4).*sign(res);
	return;
end;


if max(abs(res))>2.5
%just one ambiguity is corrected, not all of them, as it can get cycled
%	amb=zeros(length(res),1);
%	idx=find(max(abs(res)));
%	amb(idx)=sign(res(idx));
	amb=(abs(res)>pi).*sign(res);
end;


%disp(amb);



