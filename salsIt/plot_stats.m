function plot_stats(coh,s0,par,spar,da_pt,lambda,png_folder,s0_thr,coh_thr,stats_file,stat_min_coh,stat_step_coh,stat_max_s0,stat_step_s0)
%function plot_stats(coh,s0,par,spar,da_pt,lambda,png_folder,s0_thr,coh_thr,stats_file,stat_min_coh,stat_step_coh,stat_min_s0,stat_step_s0)
%a part of the salsIt (small area least-squares InSAR tool) package
%serves to plot the result statistics into separate pngs in the png_folder
%the thresholds s0_thr, coh_thr are not obligatory and are not applied in default, but if defined, both are applied (AND)
%for plotting coh/accuracy, stat_min_coh and stat_coh_step are used (by default coh_thr/0.6 and 0.01)
%for plotting accuracy plots, stat_max_s0 and stat_step_s0 are used (by default s0_thr/2.0 and 0.1)
%stats_file is a file where the accuracy/precision statistics are written (not mandatory)
%by IvCa, ivca@insar.cz, Feb 2018

if exist('s0_thr') && isnumeric(s0_thr) && s0_thr>0 && ~exist('coh_thr'),
        exp_ind=find(s0<=s0_thr);
%       disp('s0 thresholding')
%       disp([length(exp_ind) s0_thr]);
end;


if exist('coh_thr') && isnumeric(coh_thr) && coh_thr>0 && ~exist('s0_thr'),
        exp_ind=find(coh>=coh_thr);
%       disp('coh thresholding');
%       disp([length(exp_ind) coh_thr]);
end;

if exist('s0_thr') && isnumeric(s0_thr) && s0_thr>0 && exist('coh_thr') && isnumeric(coh_thr) && coh_thr>0,
        exp_ind=find((s0<=s0_thr).*(coh>=coh_thr));
end;

if exist('exp_ind'),
	coh=coh(exp_ind);
	par=par(:,exp_ind);
	spar=spar(:,exp_ind);
	s0=s0(exp_ind);
	da_pt=da_pt'(exp_ind);
end;

if ~exist('stat_min_coh'),
	if ~exist('coh_thr'),
		stat_min_coh=coh_thr;
	else
		stat_min_coh=0.6;
	end;
end;

if ~exist('stat_step_coh'),
	stat_step_coh=0.01;
end;

if ~exist('stat_max_s0'),
	if ~exist('s0_thr'),
		stat_max_s0=s0_thr;
	else
		stat_max_s0=2.0;
	end;
end;

if ~exist('stat_step_s0'),
	stat_step_s0=-0.1;
end;

%the stat_step_s0 must be negative!!
if stat_step_s0>0,
	stat_step_s0=-stat_step_s0;
end;

	
%creating the directory, if it does not exist yet
if exist(png_folder)~=7,
                mkdir(png_folder);
end;

%relation between s0 and coh
figure();
plot(s0,coh,'.');
xlabel('unique standard deviation s0 [rad]');
ylabel('coherence');
title('Relation between unique standard deviation and coherence');
fname=sprintf('%s/s0_coh_rad.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);


%the same in mm
figure();
plot(s0/4/pi*lambda,coh,'.');
xlabel('unique standard deviation s0 [mm]');
ylabel('coherence');
title('Relation between unique standard deviation and coherence');
fname=sprintf('%s/s0_coh_mm.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);





%s0 histograms
figure();
hist(s0,100);
xlabel('unique standard deviation s0 [rad]');
title('Unique standard deviation histogram');
fname=sprintf('%s/s0_hist_rad.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);

figure();
hist(s0/4/pi*lambda,100);
xlabel('unique standard deviation s0 [mm]');
title('Unique standard deviation histogram');
fname=sprintf('%s/s0_hist_mm.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);


%coh histogram
figure();
hist(coh,100);
xlabel('coherence');
title('Coherence histogram');
fname=sprintf('%s/s0_hist_coh.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);


%vel histogram
figure();
hist(par(1,:),100);
xlabel('velocity [mm/y]');
title('Velocity histogram');
fname=sprintf('%s/vel_hist.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);

%height correction histogram
figure();
hist(par(2,:),100);
xlabel('height correction [m]');
title('Height correction histogram');
fname=sprintf('%s/heicorr_hist.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);


%the relation between vel and sigma_vel
figure();
plot(par(1,:),spar(1,:),'.');
xlabel('velocity [mmm/y]');
ylabel('velocity stddev [mm/y]');
title('Relation between velocity and its standard deviation');
fname=sprintf('%s/vel_svel.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);


%the relation between hcorr and sigma_h
figure();
plot(par(2,:),spar(2,:),'.');
xlabel('height correction [m]');
ylabel('height stddev [m]');
title('Relation between height correction and its standard deviation');
fname=sprintf('%s/hei_shei.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);

%the relation between s0 and d_a
figure();
plot(s0,da_pt,'.');
xlabel('unique standard deviation [rad]');
ylabel('amplitude dispersion (d_a)');
title('Relation between unique standard deviation and amplitude dispersion');
fname=sprintf('%s/s0_da.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1);
close(f);

%the relation between coh and d_a
figure();
plot(coh,da_pt,'.');
xlabel('coherence');
ylabel('amplitude dispersion (d_a)');
title('Relation between coherence and amplitude dispersion');
fname=sprintf('%s/coh_da.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1);
close(f);



%now the statistics between coherence and empirical velocity stddev, and also the theoretical sigma_vel
coh_int=stat_min_coh:stat_step_coh:1;
k=1;
for i=1:length(coh_int)-1,
	q=find((coh>=coh_int(i)).*(coh<coh_int(i+1)));
	if q,
		n_pts_c(k)=length(q);
		v_mean_c(k)=mean(par(1,q));
		v_std_c(k)=std(par(1,q));
		sv_mean_c(k)=mean(spar(1,q));
		%now the same with the exclusion of biased measurements
		[v_mean_excl_c(k),v_std_excl_c(k),n_pts_excl_c(k),ind_val]=stats_excl(par(1,q),3); %the coefficient of exclusion is fixed here to 3
		sv_mean_excl_c(k)=mean(spar(1,q(ind_val)));
		p_coh(k)=coh_int(k)+stat_step_coh/2;
		k=k+1;
	end;
end;

%relation between coherence, precision and accuracy
figure();
plot(p_coh,sv_mean_excl_c,'b-'); hold on;
plot(p_coh,v_std_excl_c,'r-');
legend({'Precision','Accuracy'});
xlabel('Coherence');
ylabel('Velocity precision/accuracy [mm/y] ');
title('Relation between coherence and velocity precision/accuracy');
fname=sprintf('%s/coh_prec_acc.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);

k=1;
s0_int=stat_max_s0:stat_step_s0:0;
for i=1:length(s0_int)-1,
        q=find((s0<=s0_int(i)).*(s0>s0_int(i+1)));
	if q,
	        n_pts_s(k)=length(q);
        	v_mean_s(k)=mean(par(1,q));
	        v_std_s(k)=std(par(1,q));
        	sv_mean_s(k)=mean(spar(1,q));
        %now the same with the exclusion of biased measurements
	        [v_mean_excl_s(k),v_std_excl_s(k),n_pts_excl_s(k),ind_val]=stats_excl(par(1,q),3); %the coefficient of exclusion is fixed here to 3
        	sv_mean_excl_s(k)=mean(spar(1,q(ind_val)));
	        p_s0(k)=s0_int(i)+stat_step_s0/2;
		k=k+1;
	end;
end;

%relation between s0, and velocity precision and accuracy
figure();
plot(p_s0,sv_mean_excl_s,'b-'); hold on;
plot(p_s0,v_std_excl_s,'r-');
legend({'Precision','Accuracy'});
xlabel('Unique standard deviation s0');
ylabel('Velocity precision/accuracy [mm/y] ');
title('Relation between s0 and velocity precision/accuracy');
fname=sprintf('%s/s0_prec_acc.png',png_folder);
f=gcf();
print(f,fname,'-dpng','-r60');
pause(1); %to have time to finish displaying the picture and to save it
close(f);





if exist('stats_file'),
	header={'COH','N_PTS','N_PTS_VALID','V_MEAN','V_MEAN_VALID','V_ACC','V_ACC_VALID','V_PREC','V_ACC_VALID/V_PREC'};
	formatstring='%.2f,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n';
	data=[p_coh',n_pts_c',n_pts_excl_c',v_mean_c',v_mean_excl_c',v_std_c',v_std_excl_c',sv_mean_excl_c',v_std_excl_c'./sv_mean_excl_c'];
	export_csv(data',header,formatstring,stats_file);

end;
