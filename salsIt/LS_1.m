function [par,s0,spar,res,ph_dif_corr]=LS_1(ph_dif,Dm,N1,N1d,corrections)
%function [par,s0,spar,res,ph_dif_corr]=LS_1(ph_dif,Dm,N1,N1d,corrections)
%part of the salsIt (small area least-squares InSAR tool) package
%performs the least-squares processing of one pair/double
%ph_dif_corr here is corrected for ambiguities
%corrections (default 1) is not mandatory: if corrections=1, ambiguity corrections can be made
%setting corrections to 0, the ambiguity changes are forbidden
%by IvCa, ivca@insar.cz, Jan/Feb 2018

if ~exist('corrections'),
	corrections=1;
end;

k=length(ph_dif); 


[i,K]=size(Dm); %K is the number of estimated parameters

par=zeros(K,1);
%s0=zeros(1,n);
spar=zeros(K,1);
res=zeros(k,1);
ph_dif_corr=zeros(k,1);

cor=sqrt(diag(N1)); %correlation matrix

ph_dif_corr=ph_dif;

	%disp(i);
dph=ph_dif;
%disp(size(N1d))
%disp(size(dph))
un=N1d*dph;
%	disp(size(un));
r=dph-Dm*un;
amb=zeros(k,1);

%x=0;
if corrections,
	while max(abs(r))>pi
	        amb=amb-amb_corr_it(r);
        	dph=ph_dif+amb*2*pi; %in this way, because amb already contains all previously corrected ambiguities
	        un=N1d*dph;
	        r=dph-Dm*un;
		ph_dif_corr=dph;
%		x=x+1;
	end;
end;
%disp(sprintf('%d corrections',sum(abs(ph_dif_corr-ph_dif)/2/pi)));
%	disp(size(un));	
par=un;
s0=sqrt(r'*r/(k-K));
res=r;
spar=s0*cor;


%disp(sum(ph_dif_corr-ph_dif)/2/pi);
