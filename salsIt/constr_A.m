function [A,Bp,Bt,Dm,N,N1,N1d]=constr_A(dates,itf_dates,master_date,bp,lambda,sl_r,theta,file_logN)
%function [A,Bp,Bt,Dm,N,N1,N1d]=constr_A(dates,itf_dates,master_date,bp,lambda,sl_r,theta,file_logN)
%part of the salsIt (small area least-squares InSAR tool)
%constructs the design matrices for processing
%dates and itf_dates can be the same (for PS), but also different (for SB)
%bp,bt are perpendicular and temporal baselines
%lambda [mm] is the satellite wavelength
%sl_r is the slant range (approximate, here the value of the center of the crop) - used just for computation of height corrections
%theta is the incidence angle, also approximate for the crop center, in degrees
%file_logN is not mandatory: it is a file where N can be logged, together with correlations between the time/bperp/temperature vectors
%by IvCa, ivca@insar.cz, Jan/Feb 2018



%here, we have to align itf_dates to dates, as generally, the two vectors can be different
%and Bp is aligned with itf_dates, by default (??? by stamps)

if issorted(dates) && issorted(itf_dates),
	Bp=bp';
	A=eye(length(dates));
	Bt=dates-master_date;

else
	disp('unsorted dates or itf_dates, not implemented yet!!!')
	A=zeros(length(dates));
	Bp=bp;
	Bt=dates-master_date;
end;


%lambda is in mm here
Dm=[Bt'/365*4*pi/lambda,Bp'/sl_r/sin(theta/180*pi)*4*pi/lambda*1000];

%tady by se mely hlidat jeste hodnoty v tech maticich, aby se to pak neblizilo singularnim maticim!!! (zatim neimplementovano) 


N=Dm'*Dm;
N1=inv(N);
N1d=N1*Dm';

if exist('file_logN'),

	[p,q]=size(N1);
	f=fopen(file_logN,'w');
	fprintf(f,'normal matrix inversion for this dataset\n');
	for i=1:p,
		for j=1:q,
			fprintf(f,'%f ',N1(i,j));
		end;
		fprintf(f,'\n');
	end;
	%here, p=q by definition
	fprintf(f,'\n velocity/height correlation: %f\n',sqrt(N1(1,2))/sqrt(N1(1,1))/sqrt(N1(2,2)));
	if p==3,
		fprintf(f,'velocity/temperature correlation: %f\n',sqrt(N1(1,3))/sqrt(N1(1,1))/sqrt(N1(3,3)));
		fprintf(f,'height/temperature correlation: %f\n',sqrt(N1(2,3))/sqrt(N1(2,2))/sqrt(N1(3,3)));
	end;

	fprintf(f,'\n number of degrees of freedom: %d',length(dates)-p);

	fclose(f);
end;
