function [itf,da_thr,itf_dates,cal_c,inc_angle]=load_data(selpsc_file,incangle_file,height,scene_mask)
%function [itf,da_thr,itf_dates,cal_c,inc_angle]=load_data(selpsc_file,incangle_file,height,scene_mask)
%part of the salsIt (small area least-squares InSAR tool) package
%loads the interferogram data (from stamps pre-processing)
%scene_mask is either a vector (1 for valid itfs, 0 for invalidated ones), or a file with the vector (each number on a separate line)
%scene_mask is not mandatory; used to possibly invalidate low-coherent itfs
%by IvCa, ivca@insar.cz, Jan/Feb 2018

if exist('scene_mask')~=1,
	take_all_scenes=1;
%	disp('neexistuje');
else
	take_all_scenes=0;
	if isnumeric(scene_mask),
		sc_m=scene_mask;
	else
		sc_m=importdata(scene_mask);
	end;
end;



f=fopen(selpsc_file);

da_thr=fscanf(f,'%f',1);

w=fscanf(f,'%d',1);

i=0;
k=0;
while ~feof(f),
	fname=fscanf(f,'%s',1);
	k=k+1;  %k is over all filenames, while i is only over the valid files (for sc_m)
%loading the data
	if length(fname)>0 && (take_all_scenes==1 || sc_m(k)>0),
		i=i+1;
		cal_c(i)=fscanf(f,'%f',1);
		itf(i,:,:)=freadbk(fname,height,'cpxfloat32');
		s=pwd;
		ind=strfind(fname,s);
		itf_dates(i)=datenum(substr(fname,ind+length(s)+1,8),'yyyymmdd'); %the +1 is here due to the '/' which is not part of the s string (from pwd)
	end;

end;


incangle=freadbk(incangle_file,height,'float32');

inc_angle=mean(incangle(:));
