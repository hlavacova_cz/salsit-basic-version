function [lat,lon,mean_ampl,orig_height,heading]=load_auxdata(lat_file,lon_file,height_file,meanampl_file,height,heading_file)
%function [lat,lon,mean_ampl,orig_height,heading]=load_auxdata(lat_file,lon_file,height_file,meanampl_file,height,heading_file)
%part of the salsIt (small area least-squares InSAR tool) package
%loads lat, lon, height and mean amplitude of the crop from binary files
%heading (output) is in degrees
%uses the freadbk.m script, downloaded from TU Delft
%by IvCa, ivca@insar.cz, Feb 2018


lat=freadbk(lat_file,height,'float32');
lon=freadbk(lon_file,height,'float32');
mean_ampl=freadbk(meanampl_file,height,'float32');
orig_height=freadbk(height_file,height,'float32');


heading=importdata(heading_file);


