function [refpt,id]=select_refpt(refpt_file,d_a,write_to_file,pts,npt_check)
%function refpt=select_refpt(refpt_file,d_a,write_to_file)
%part of the salsIt (small area least-squares InSAR tool) package
%selects a reference point, either reading from a file (if the file exists), or by d_a (one of the npt_check points with lowest d_a, which is the most close to the area center
%npt_check is not mandatory (default 10)
%if write_to_file is (not mandatory), the result is written to the refpt_file
%pts is the point coordinates vector, in which id is found
%not mandatory, but if not specified, id is 0
%by IvCa, ivca@insar.cz, Jan 2018

if ~exist('npt_check'),
	npt_check=10;
end;

if exist(refpt_file)==2,
	q=importdata(refpt_file);
	i=q(1);
	j=q(2);
	if exist('d_a'),
		disp(sprintf('refpoint taken from file %s: az=%d, ran=%d, D_A=%f\n',refpt_file,i,j,d_a(i,j)));
	else 
		disp(sprintf('refpoint taken from file %s: az=%d, ran=%d, D_A not defined\n',refpt_file,i,j));
	end;
	refpt=[i,j]; 
else
	if length(pts)<npt_check,
		disp(sprintf('only %d points are checked as there are no more points',length(pts)));
		npt_check=length(pts);
	end;
	dat=sort(d_a(:))(npt_check); %the 10th lowest value of D_A
	[k,l]=find(d_a<=dat);
	[p,q]=size(d_a); % the size of the crop
	for x=1:10,
		dist(x)=sqrt((k(x)-p/2)^2+(l(x)-q/2)^2); %distance from the center of the crop
	end;
	[m,n]=min(dist);
	i=k(n);
	j=l(n);
	disp(sprintf('refpoint selected on the basis of D_A and distance from crop center: az=%d, ran=%d, D_A=%f\n',i,j,d_a(i,j)));
	if exist('write_to_file') && write_to_file==1,
		f=fopen(refpt_file,'w');
		fprintf(f,'%d,%d',i,j);
		fclose(f);
	end;
end;

refpt=[i,j];
id=0;

if exist('pts'),
	i=find(pts(:,1)==refpt(1));
	j=find(pts(:,2)==refpt(2));
	id=intersect(i,j);
end;


	
