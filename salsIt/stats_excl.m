function [m,s,n,ind]=stats_excl(v,coef)
%function [m,s,n,ind]=stats_excl(v,coef)
%part of the salsIt (small area least-squares InSAR tool) package
%computes the statistics (mean, standard deviation) of the vector v
%with the exclusion of biased measurements, where abs(v(i)-m)>coef*s
%n is the number of unbiased measurements, ind are their indicies in v
%by IvCa, ivca@insar.cz, Feb 2018

m=mean(v);

s=std(v);

ind=find(abs(v-m)<=coef*s);

n=length(ind);

m=mean(v(ind));

s=std(v(ind));


