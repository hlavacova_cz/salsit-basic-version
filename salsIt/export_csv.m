function export_csv(data,header,formatstrings,filename)
%function export_csv(data,header,formatstrings,filename)
%exports the header/data to the filename using format defined by dormatstrings
%formatstrings is a string, e.g. '%d,%.1f,%d,%f\n' (for 4-column data)
%if header is not to be written, it is expected to be ''

f=fopen(filename,'w');

hline='';

if length(header)>0

	for i=1:length(header)-1,
		hline=strcat(hline,header{i});
		hline=strcat(hline,', ');
	end;
	hline=strcat(hline,header{end});
	hline=strcat(hline,'\n');

	%disp(hline)
	fprintf(f,hline);
end;


fprintf(f,formatstrings,data); %should work like this
%disp(data(:,1))

fclose(f);

