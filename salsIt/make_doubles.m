function [ph_dif,s_phdif,doubles,s_doubles,ind,b_ind]=make_doubles(pts,pt_data,png_image,logfile)
%function [ph_dif,s_phdif,doubles,s_doubles,ind,b_ind]=make_doubles(pts,pt_data,png_image,logfile)
%part of the salsIt (small area least-squares InSAR tool) package
%creates point pairs based on the Delaunay triangulation
%possibly prints the triangulation graph into a defined png image (not mandatory)
%the resulting doubles, triangles both contain indices into pts 
%s_doubles are sorted doubles, and unified
%pt_data are point data, only phase data (2D, float32, radians)
%ind and b_ind are forward and back indices between doubles and s_doubles (and ph_dif and s_phdif) to be used later again
%png_image and logfile are not mandatory
%into png_image, a graph of connections between points is plotted
%into logfile, triangles with possible ambiguity problems are logged (it seems to be useless... happens for smaller values of D_A threshold from selpsc.in, with larger triangles)
%by IvCa, ivca@insar.cz, Jan 2018


triangles=delaunay(pts(:,1),pts(:,2));
%the results of tri are three indexes into pts
%now converting them to pts and making them unique

n=length(triangles);  %number of triangles

[k,l]=size(pt_data); %k is the number of itfs, l is the number of points

doubles=zeros(3*n,2);
ph_dif=zeros(k,3*n);
val_tri=zeros(k,n);
val_dou=zeros(k,3*n);

if exist('logfile')==1,
	f=fopen(logfile,'w');
end;


for i=1:n,
	p=(i-1)*3+1;
	q=i*3;
	doubles(p:q,:)=[triangles(i,1),triangles(i,2); triangles(i,2),triangles(i,3);triangles(i,3), triangles(i,1)];
	ph_dif(:,p:q)=[wrap(pt_data(:,triangles(i,2))-pt_data(:,triangles(i,1))),wrap(pt_data(:,triangles(i,3))-pt_data(:,triangles(i,2))),wrap(pt_data(:,triangles(i,1))-pt_data(:,triangles(i,3)))];


%this is probably useless for PS with one common master
%but maybe, it can be useful for SBAS with filtered data???
	
	s=sum(ph_dif(:,p:q),2)/2/pi;
	val_tri(:,i)=s;
	val_dou(:,p:q)=s*ones(1,3);
	if sum(abs(s))>0.5 && exist('f')==1,
		fprintf(f,'possible ambiguity problem for triple %d, points %d, %d, %d, interferograms %d', i, triangles(i,1), triangles(i,2), triangles(i,3),find(abs(s)>0.5));
	end;

end;


%doubles will not be sorted with regard to the reference point
%now unifying
[s_doubles,ind,b_ind]=unique(doubles,'rows');

s_phdif=ph_dif(:,ind);

if exist('f'),
	fclose(f);
end;

	
if exist('png_image'),
	f=figure();
	triplot(triangles,pts(:,1),pts(:,2));
	f=gcf();
%	set(f,'PaperUnits','centimeters');
%	set(f,'PaperPosition',[0 0 21.3 10]);
	print(f,png_image,'-dpng','-r60');
	%maybe the waiting time should be dependent on the image complexity (???)
	pause(5);   %waiting time, otherwise the image is not exported
	%if the image is not complete, it helps not to close the image at all
	close(f);
end;






