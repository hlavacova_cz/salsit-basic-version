function [h_pt,h_crop]=height_corr(par_pt,orig_height,pts,file_out,validity)
%function [h_pt,h_crop]=height_corr(par_pt,orig_height,pts,file_out,validity)
%part of the salsIt (small area least-squares InSAR tool) package
%performs the height correction (for export, ideally also for lat/lon correction)
%orig_height is a matrix (same as h_crop) for all points, while par_pt and h_pt are vectors just for processed points
%pts is the matrix of point coordinates - to find the position of all processed points in the matrices
%the file_out is not mandatory - if present, the result is written to this (binary) file, correction applied only for the processed points
%validity is neither mandatory - it allows us to filter out the processed points where the correction should not be performed (e.g. s0<s0_thr), 1 for valid points, 0 for nonvalid
%by IvCa, ivca@insar.cz, Feb 2018

[n_pt,xx]=size(pts);

if ~exist('validity'),
	validity=ones(n_pt,1);
end;

h_crop=orig_height;

for i=1:n_pt,
	h_pt(i)=validity(i)*par_pt(2,i)+orig_height(pts(i,1),pts(i,2));
	h_crop(pts(i,1),pts(i,2))=validity(i)*par_pt(2,i)+orig_height(pts(i,1),pts(i,2));
end;


if exist('file_out')==1,
	fwritebk(h_crop,file_out,'float32');
end;
	

