function [pts,pt_data,d_a,da_pt]=sel_pts(data,da_thr,cal_c,mask_file,height,da_file)
%function [pts,pt_data,d_a,da_pt]=sel_pts(data,da_thr,cal_c,mask_file,height,da_file)
%part of the salsIt (small area least-squares InSAR tool) package
%selects the points to be processed, based on the amplitude dispersion
%mask_file is not obligatory 
%height is needed when mask_file is specified, in order to read it
%pt_data is now only phase data
%pts are two column vectors (2D matrix) of x and y coordinates
%d_a is specified for all points, i.e. has the same dimension as original data (except for the first dimension, specifying number of itfs)
%if da_file is specified, d_a is not calculated, but loaded from the specified file, with the same height as mask_file
%da_pt is a vector of D_A for selected points
%by IvCa, ivca@insar.cz, Jan 2018

[k,l,m]=size(data);

if exist('da_file')==1,
	d_a=freadbk(da_file,height,'float32');
else
	n=length(cal_c); %number of itfs

	for i=1:n,
		cal_data(i,:,:)=data(i,:,:)/cal_c(i);
	end;

	da=sqrt(var(abs(cal_data),1,1))./mean(abs(cal_data),1);

	%resizing
	d_a=zeros(l,m);
	for i=1:l,
		for j=1:m,
			d_a(i,j)=da(1,i,j);
		end;
	end;
end;

val_pts=(d_a<=da_thr);


if exist('mask_file'),
	mask=freadbk(mask_file,height,'uchar');

	val_pts=val_pts.*(1-mask);

end;


[i,j]=find(val_pts);

pts=[i,j];

q=length(i);

pt_data=zeros(k,q);

for x=1:q,
	pt_data(:,x)=angle(data(:,i(x),j(x)));
	da_pt(x)=d_a(i(x),j(x));
end;

disp(sprintf('%d points selected for processing (out of %d pixels, %.1f %%) with D_A< %.2f',length(i),l*m,length(i)/l/m*100,da_thr));


