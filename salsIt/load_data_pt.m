function [pts,pt_data,d_a,da_thr,da_pt,itf_dates,cal_c,inc_angle]=load_data_pt(selpsc_file,incangle_file,height,da_file,im_mask,scene_mask)
%function [pts,pt_data,da_thr,da_pt, itf_dates,cal_c,inc_angle]=load_data_pt(selpsc_file,incangle_file,height,da_file,im_mask,scene_mask)
%part of the salsIt (small area least-squares InSAR tool) package
%loads the interferogram data (from stamps pre-processing)
%and at the same time, makes the pt_data (to save memory and avoid swapping), the da_file is necessary to be present already 
%scene_mask is either a vector (1 for valid itfs, 0 for invalidated ones), or a file with the vector (each number on a separate line)
%scene_mask is not mandatory; used to possibly invalidate low-coherent itfs
%by IvCa, ivca@insar.cz, Jan-Mar 2018

if exist('scene_mask')~=1,
	take_all_scenes=1;
%	disp('neexistuje');
else
	take_all_scenes=0;
	if isnumeric(scene_mask),
		sc_m=scene_mask;
	else
		sc_m=importdata(scene_mask);
	end;
end;


d_a=freadbk(da_file,height,'float32');

f=fopen(selpsc_file);

da_thr=fscanf(f,'%f',1);

val_pts=(d_a<=da_thr);

if exist('im_mask')==1 && exist(im_mask)==2,
        mask=freadbk(im_mask,height,'uchar');
        val_pts=val_pts.*(1-mask);

end;

[r,s]=find(val_pts);

pts=[r,s];

q=length(r)

for x=1:q,
	da_pt(x)=d_a(r(x),s(x));
end;

w=fscanf(f,'%d',1);

i=0;
k=0;
while ~feof(f),
	fname=fscanf(f,'%s',1);
	k=k+1;  %k is over all filenames, while i is only over the valid files (for sc_m)
%loading the data
	if length(fname)>0 && (take_all_scenes==1 || sc_m(k)>0),
		i=i+1;
		cal_c(i)=fscanf(f,'%f',1);
		itf=freadbk(fname,height,'cpxfloat32');
		for x=1:q,
			pt_data(i,x)=angle(itf(r(x),s(x)));
		end;
		st=pwd;
		ind=strfind(fname,st);
		itf_dates(i)=datenum(substr(fname,ind+length(st)+1,8),'yyyymmdd'); %the +1 is here due to the '/' which is not part of the s string (from pwd)
	end;

end;


incangle=freadbk(incangle_file,height,'float32');

inc_angle=mean(incangle(:));

%disp(size(inc_angle));
