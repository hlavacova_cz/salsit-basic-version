%the salsIt (small area least-squares InSAR tool) package - the script to make the whole PS (LS in this version) processing
%all the parameters here and files are to change
%first, set the paths
%the freadbk.m script from the Delft insar package is needed
%by IvCa, ivca@insar.cz, Jan/Feb 2018

format short g;

[w,h,bp,dates,lambda,master_date,sl_r]=load_metadata('width.txt','len.txt','bperp.1.in','day.1.in','lambda.1.in','master_day.1.in','../metadata.txt','crop.txt','rangepixelsize.1.in');

[itf,da_thr,itf_dates,cal_c,inc_angle]=load_data('selpsc.in','look_angle.raw',h);
[pts,pt_data,d_a,da_pt]=sel_pts(itf,da_thr,cal_c,'mask.ij',h);
%these two lines are useful when the da.flt is not available, however, it has high memory requirements, and for extensive areas, it may lead to extensive (useless) swapping


%[pts,pt_data,d_a,da_thr,da_pt,itf_dates,cal_c,inc_angle]=load_data_pt('selpsc.in','look_angle.raw',h,'da.flt');
%this works only with pre-generated da.flt


[A,Bp,Bt,Dm,N,N1,N1d]=constr_A(dates,itf_dates,master_date,bp,lambda,sl_r,inc_angle,'matrix_N1.txt');


%pt_data are now only phase data (in 2D)

[refpt,id_rp]=select_refpt('refpt.txt',d_a,1,pts,20);
%if you want your own ref point, just edit the refpt.txt file


%clear itf, cal_c;

[ph_dif,s_phdif,doubles,s_doubles,ind,b_ind]=make_doubles(pts,pt_data,'triangles.png');

[par_s_doubles,s0_s_doubles,spar_s_doubles,res_s_doubles,s_ph_dif_corr]=LS_doubles(s_doubles,s_phdif,Dm,N1,N1d,'hist_doubles',lambda);
%this step can be easily parallelized, every double can be processed independently


[unw_pt,par_pt,s0_pt,spar_pt,res_pt]=integrate_LS(s_phdif,s0_s_doubles,s_doubles,id_rp,Dm,N1,N1d);

unw_pt=-unw_pt; %this correction seems necessary, but no mistake like that was found
%par_pt=-par_pt; %how about here? this seems ok
%res_pt=-res_pt; %and here? also ok

%try unw_pt(:,1)-(pt_data(:,1)-pt_data(:,id_rp)) for any point: the result must be a vector of zeros and 2pis

%the Shenzen algorithm (not recommended for geodetical applications)
%res_pt=shenzen(res_pt);


coh_pt=get_coh(res_pt,'hist_coh_LS.png');


s0_thr=1.2; %in radians
%for 99.5% (??) probability (t=3) of avoiding unwrapping errors, the threshold of 1.05 rad is needed (corresponds to coh 0.6 ?? but this is the inner precision)
%for 95% (??) probability (t=2) of avoiding unwrapping errors, the threshold of 1.57 is needed
coh_thr=0.6;
%s0 is more sensitive than coherence to biased measurements, e.g. due to the snow


[lat,lon,mean_ampl,orig_height,heading]=load_auxdata('lat','lon','hgt','mean_amp.flt',h,'heading.1.in');

[h_pt,h_crop]=height_corr(par_pt,orig_height,pts,'height_corr',s0_pt<s0_thr);


export_results(pts,lat,lon,orig_height,par_pt,res_pt,s0_pt,spar_pt,coh_pt,mean_ampl,d_a,dates,lambda,master_date,id_rp,'resultLS_par.csv','resultLS_res.csv','resultLS_mov.csv','resultLS_model.csv','refpt_pos.csv',s0_thr,coh_thr);

save results_LS.mat

plot_stats(coh_pt,s0_pt,par_pt,spar_pt,da_pt,lambda,'stats_final_LS',s0_thr,coh_thr,'prec_acc_LS.stats');


