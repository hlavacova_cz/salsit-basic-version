function [unw_pt,par_pt,s0_pt,spar_pt,res_pt]=integrate_LS(s_phdif,s0_s_doubles,s_doubles,id_rp,Dm,N1,N1d)
%function [unw_pt,par_pt,s0_pt,spar_pt,res_pt]=integrate_LS(s_phdif,s0_s_doubles,s_doubles,id_rp,Dm,N1,N1d)
%the essential part of the salsIt (small area least-squares InSAR tool) package
%integrates the network results, and recomputes them at the same time w.r.t. the ref point
%ambiguity corrections are still performed here, resulting in very reasonable standard deviations
%s_phdif are phase differences (already unwrapped) between close points (delaunay network), the network is defined by s_doubles
%s0_s_doubles are standard deviations of each double (line in the del. network)
%id_rp is the ID of the ref point
%Dm, N1,N1d are matrices created by constr_A.m
%by IvCa, ivca@insar.cz, Feb 2018


[i,K]=size(Dm); %K is the number of estimated parameters

n_pt=max(max(s_doubles)); %the number of points
[n_itf,n_d]=size(s_phdif);
[n_par,n_parr]=size(N1); %n_par is the number of parameters to estimate


par_pt=zeros(n_par,n_pt);
proc_pt=zeros(n_pt,1);
unw_pt=zeros(n_itf,n_pt); %unwrapped phase, expected to be spatially consistent
s0_pt=zeros(n_pt,1);
par_pt=zeros(K,n_pt);
spar_pt=zeros(K,n_pt);
res_pt=zeros(n_itf,n_pt);


q_pt=id_rp; %creating a queue of processed points
rfr_pt=0; %creating a queue of referring points, w.r.t. which the queued points should be processed
rfr_sd=0; %a queue of referring (sorted) doubles
s0_2=0; % weights for the queue, as sum of s0^2
l_qpt=1;
proc_pt(id_rp)=1;
%par_pt, unw_pt, res_pt and s0_pt should be 0 for the ref. point

while l_qpt>0,


	%processing the first point of the queue (the one with lowest s0_2)
	proc_pt(q_pt(1))=1;
	if q_pt(1)~=id_rp,
%		disp([size(q_pt) size(rfr_pt) size(rfr_sd)]);
		%this is processing w.r.t. the REF POINT directly!!!!
		%but the ambiguities are first estimated from the delaunay network
		unw_pt_1est=unw_pt(:,abs(rfr_pt(1)))-sign(rfr_pt(1))*s_phdif(:,(abs(rfr_sd(1))));
		[par_ls,s0_ls,spar_ls,res_ls,unw_pt_ls]=LS_1(unw_pt_1est,Dm,N1,N1d); %ambiguity corrections allowed here
		unw_pt(:,q_pt(1))=unw_pt_ls;
		s0_pt(q_pt(1))=s0_ls;
	 	spar_pt(:,q_pt(1))=spar_ls;
		par_pt(:,q_pt(1))=par_ls;
		res_pt(:,q_pt(1))=res_ls;
		s0_2(1)=s0_ls^2;
%i don't understand why the minus signs here, but the results are much more reasonable like this
	end;
	

	idx1=find(s_doubles(:,1)==q_pt(1)); 
	idx2=find(s_doubles(:,2)==q_pt(1)); %these are all lines from the first point of the queue
						%this does not work with lookup function (maybe, just one row is found). in addition, with find it is much faster!!!

	%disp([l_qpt q_pt(1) rfr_pt(1) rfr_sd(1) sqrt(s0_2(1)) sum(proc_pt)]);
	for j=1:length(idx1),
		q2=s_doubles(idx1(j),2);
		if ~proc_pt(q2), %i.e. if the point is not yet processed
			idk_q=find(q_pt==q2);
			if idk_q, %i.e. if the point is already in the queue
				s_test=s0_2(1)+s0_s_doubles(idx1(j))^2;
				if s_test<s0_2(idk_q)^2,  %the reference to the point should be exchanged
	                                rfr_pt(idk_q)=q_pt(1);
        	                        rfr_sd(idk_q)=idx1(j);
                	                s0_2(idk_q)=s0_2(1)+s0_s_doubles(idx1(j))^2;
				end; %if not, nothing should be performed: the point is already in the list with better weight
			else
				q_pt=[q_pt;q2];
				rfr_pt=[rfr_pt;q_pt(1)];
				rfr_sd=[rfr_sd;idx1(j)];
				s0_2=[s0_2;s0_2(1)+s0_s_doubles(idx1(j))^2];
			end;
		end;
	end;

	for j=1:length(idx2), 
		q1=s_doubles(idx2(j),1);
		if ~proc_pt(q1),
			idk_q=find(q_pt==q1);
                        if idk_q, %i.e. if the point is already in the queue
				s_test=s0_2(1)+s0_s_doubles(idx2(j))^2;
				if s_test<s0_2(idk_q)^2, %the reference to the point should be exchanged
					rfr_pt(idk_q)=-q_pt(1);
					rfr_sd(idk_q)=-idx2(j);
					s0_2(idk_q)=s0_2(1)+s0_s_doubles(idx2(j))^2;
				end; %if not, nothing should be performed: the point is already in the list with better weight
			else
				q_pt=[q_pt;q1];  %the point is now at the end of the queue
				rfr_pt=[rfr_pt;-q_pt(1)];
				rfr_sd=[rfr_sd;-idx2(j)]; %negative sign to sign that the unwrapped phase has to be added (instead of subtracting), the way to differentiate this case w.r.t. the previous one
				s0_2=[s0_2;s0_2(1)+s0_s_doubles(idx2(j))^2];
			end;
		end;
	end;


	q_pt=q_pt(2:end); %excluding the first point from the queue
	rfr_pt=rfr_pt(2:end);
	rfr_sd=rfr_sd(2:end);
	s0_2=s0_2(2:end);
	l_qpt=length(q_pt);
%now sorting
	[s0_2,idk]=sort(s0_2);
	q_pt=q_pt(idk);
	rfr_pt=rfr_pt(idk);
	rfr_sd=rfr_sd(idk,:);
end;


